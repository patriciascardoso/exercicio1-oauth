package br.com.mastertech.series.controllers;

import br.com.mastertech.series.models.Serie;
import br.com.mastertech.series.security.Usuario;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SerieController {

    @GetMapping("/{nome}/{temporada}")
    public Serie create(@PathVariable String nome, @PathVariable int temporada, @AuthenticationPrincipal Usuario usuario){
        Serie serie = new Serie();
        serie.setNome(nome);
        serie.setTemporada(temporada);
        serie.setDono(usuario.getName());
        return serie;
    }
}
